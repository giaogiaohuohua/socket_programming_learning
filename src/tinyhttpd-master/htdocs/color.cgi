#!/usr/bin/perl -Tw
#使用which perl把perl的地址改对

use strict;#这句话是开启严格模式，可以帮助程序员在编码过程中发现一些常见的错误
use CGI;

my($cgi) = new CGI;#定义了一个名为$cgi的变量，并使用CGI模块创建一个新的CGI对象

print $cgi->header;#将一个HTTP响应头发送到标准输出
my($color) = "blue";#定义了一个名为$color的变量，并将其初始值设为blue
$color = $cgi->param('color') if defined $cgi->param('color');#如果客户端发送了一个名为color的参数，那么就将这个参数的值赋给$color变量

print $cgi->start_html(-title => uc($color),#用CGI模块的start_html()方法向标准输出发送一个HTML文档的开头部分，其中的title参数和BGCOLOR参数的值分别为$color变量的大写形式和$color变量的值
                       -BGCOLOR => $color);
print $cgi->h1("This is $color");#使用CGI模块的h1()方法向标准输出发送一个h1标签，其中的内容为“This is $color”
print $cgi->end_html;#使用CGI模块的end_html()方法向标准输出发送一个HTML文档的结尾部分
