
#ifndef _SOCKET_WARP_H_
#define _SOCKET_WARP_H_
#include <stdio.h>//perror()
#include <sys/socket.h>//socket() listen()
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

void perr_exit(const char *s);
int Accept(int fd, struct sockaddr *sa, socklen_t *salenptr);
int Bind(int fd, const struct sockaddr *sa, socklen_t salen);
int Connect(int fd, const struct sockaddr *sa, socklen_t salen);
int Listen(int fd, int backlog);
int Socket(int family, int type, int protocol);
ssize_t Read(int fd, void *ptr, size_t nbytes);
ssize_t Write(int fd, const void *ptr, size_t nbytes);
int Close(int fd);
ssize_t Readn(int fd, void *vptr, size_t n);
ssize_t Writen(int fd, const void *vptr, size_t n);
ssize_t Readline(int fd, void *vptr, size_t maxlen);

#endif //_SOCKET_WARP_H_

//typedef long ssize_t(多加一个s表示有符号signed)
//typedef unsigned long size_t