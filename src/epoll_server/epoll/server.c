#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <errno.h>
#include <ctype.h>

#include "socket_wrap.h"

#define MAXLINE 8192
#define SERV_PORT 8002

#define OPEN_MAX 5000  //这东西好像和突破1024文件描述符限制有关

int main(int argc, char *argv[])
{
    int i, listenfd, connfd, sockfd;
    int  n, num = 0;
    ssize_t nready;//返回需要处理的事件数目
    ssize_t efd, res;
    char buf[MAXLINE], str[INET_ADDRSTRLEN];
    socklen_t clilen;

    struct sockaddr_in cliaddr, servaddr;

    //s
    listenfd = Socket(AF_INET, SOCK_STREAM, 0);
    int opt = 1;
    //端口复用
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    //b
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(SERV_PORT);
    Bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
    //l
    Listen(listenfd, 20);

    //create
    efd = epoll_create(OPEN_MAX);//创建epoll模型, efd指向红黑树根节点
    if (efd == -1)
        perr_exit("epoll_create error");

    //ctl
    struct epoll_event tep, ep[OPEN_MAX];
    tep.events = EPOLLIN; // LT 水平触发 (默认)
    tep.data.fd = listenfd;//指定lfd的监听事件为"读"

    //将lfd及对应的结构体挂上红黑树
    res = epoll_ctl(efd, EPOLL_CTL_ADD, listenfd, &tep);    
    if (res == -1)
        perr_exit("epoll_ctl error");

    for ( ; ; ) {
        /*epoll为server阻塞监听事件, ep为struct epoll_event类型数组, OPEN_MAX为数组容量, -1表永久阻塞*/
        nready = epoll_wait(efd, ep, OPEN_MAX, -1); //返回参数ep[]表示有事件的结构体，下面要分别对其进行处理
        if (nready == -1)
            perr_exit("epoll_wait error");

        for (i = 0; i < nready; i++) {
            //如果不是"读"事件, 继续循环
            if (!(ep[i].events & EPOLLIN))      
                continue;

            //请求连接事件 
            if (ep[i].data.fd == listenfd) {            
                clilen = sizeof(cliaddr);
                connfd = Accept(listenfd, (struct sockaddr *)&cliaddr, &clilen);    //接受链接

                printf("received from %s at PORT %d\n", 
                        inet_ntop(AF_INET, &cliaddr.sin_addr, str, sizeof(str)), 
                        ntohs(cliaddr.sin_port));
                printf("cfd %d---client %d\n", connfd, ++num);

                tep.events = EPOLLIN; tep.data.fd = connfd;
                res = epoll_ctl(efd, EPOLL_CTL_ADD, connfd, &tep);      //加入红黑树
                if (res == -1)
                    perr_exit("epoll_ctl error");

            //读写事件
            } else {     
                sockfd = ep[i].data.fd;
                n = Read(sockfd, buf, MAXLINE);

                //读到0,说明客户端关闭链接
                if (n == 0) {                                          
                    res = epoll_ctl(efd, EPOLL_CTL_DEL, sockfd, NULL);  //将该文件描述符从红黑树摘除  //参数4为NULL表示不传入结构体
                    if (res == -1)
                        perr_exit("epoll_ctl error");
                    Close(sockfd);                                      //关闭与该客户端的链接
                    printf("client[%d] closed connection\n", sockfd);

                //出错
                } else if (n < 0) {                                             
                    perror("read n < 0 error: ");
                    res = epoll_ctl(efd, EPOLL_CTL_DEL, sockfd, NULL);//摘除节点
                    Close(sockfd);

                //转大写,写回给客户端
                } else {                                             
                    for (i = 0; i < n; i++)
                        buf[i] = toupper(buf[i]);                     

                    Write(STDOUT_FILENO, buf, n);
                    Writen(sockfd, buf, n);
                }
            }
        }
    }
    Close(listenfd);
    Close(efd);

    return 0;
}

