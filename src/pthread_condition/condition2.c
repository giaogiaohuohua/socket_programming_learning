

#include <stdio.h>
#include<pthread.h>

//条件变量
pthread_cond_t cond;

//互斥锁需搭配条件变量才能实现生产者/消费者模型
pthread_mutex_t mutex;

//仓库大小最大为5，初始为0，不使用up所说麻烦的链表
#define RESOURCE_MAX 5
int resource = 0;

//生产者
void* producer(void *arg) {
    while (1) {

        pthread_mutex_lock(&mutex);

        resource++;
        printf("本线程%ld生产中...当前仓库资源：%d\n", pthread_self(), resource);
        pthread_mutex_unlock(&mutex);

        pthread_cond_broadcast(&cond);//唤醒所以等待线程

        Sleep(1000+rand()%1000);
    }


    return NULL;
}



//消费者
void* consumer(void* arg) {
    while (1) {

        pthread_mutex_lock(&mutex);
        while (resource==0) {//这个while似乎还没法用if,不太明白？
            //这个函数十分特殊：假如此时在这个函数卡住，此时mutex也被这个线程掌握，生产者无法打开mutex锁，会造成死锁。但是这个函数卡住时会自动打开mutex就不会死锁了
            pthread_cond_wait(&cond,&mutex);//阻塞并等待信号（这个信号由cond传递）
        }
        resource--;
        printf("本线程%ld消费中...当前仓库资源：%d\n",pthread_self(), resource);
        pthread_mutex_unlock(&mutex);

        Sleep(1000 + rand() % 1000); 
    }

    return NULL;
}


int main() {
    pthread_mutex_init(&mutex,NULL);
    pthread_cond_init(&cond, NULL);
    pthread_t c[5], p[5];//5个消费者，5个生产者
    int i = 0;

    //创建线程
    for (i = 0;i<5;++i) {
        pthread_create(&p[i], NULL, producer, NULL);//函数名就是函数的地址 
    }
    for (i = 0; i < 5; ++i) {
        pthread_create(&c[i], NULL, consumer, NULL);

    }

    //结束回收线程
    for (i = 0;i<5; ++i) {
        pthread_join(p[i],NULL);
        pthread_join(c[i], NULL);
    }


    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);
    return 0;
}
