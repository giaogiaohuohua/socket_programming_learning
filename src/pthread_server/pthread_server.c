#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>

#include "socket_wrap.h"

#define MAXLINE 8192
#define SERV_PORT 8000

struct s_info {                     //定义一个结构体, 将地址结构跟cfd捆绑
    struct sockaddr_in cliaddr;
    int cfd;
};

void *do_work(void *arg)
{
    int n,i;
    struct s_info *ts = (struct s_info*)arg;
    char buf[MAXLINE];
    char str[INET_ADDRSTRLEN];      //#define INET_ADDRSTRLEN 16  可用"[+d"查看

    while (1) {
        n = Read(ts->cfd, buf, MAXLINE);                     //读客户端
        if (n == 0) {
            printf("the client %d closed...\n", ts->cfd);
            break;                                              //跳出循环,关闭cfd
        }
        printf("received from %s at PORT %d\n",
                inet_ntop(AF_INET, &(*ts).cliaddr.sin_addr, str, sizeof(str)),
                ntohs((*ts).cliaddr.sin_port));                 //打印客户端信息(IP/PORT)

        for (i = 0; i < n; i++) 
            buf[i] = toupper(buf[i]);                           //小写-->大写

        Write(STDOUT_FILENO, buf, n);                           //写出至屏幕
        Write(ts->cfd, buf, n);                              //回写给客户端
    }
    Close(ts->cfd);

    return (void *)0;
}

int main(void)
{
    struct sockaddr_in servaddr, cliaddr;
    socklen_t cliaddr_len;
    int lfd, cfd;//门卫socket文件描述符、建立好连接的socket文件描述符
    pthread_t tid;
    struct s_info ts[256];      //创建结构体数组.
    int i = 0;
    //s
    lfd = Socket(AF_INET, SOCK_STREAM, 0);                  
    //b
    bzero(&servaddr, sizeof(servaddr));           
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);                             
    servaddr.sin_port = htons(SERV_PORT);                                     
    Bind(lfd, (struct sockaddr *)&servaddr, sizeof(servaddr));           
    //l
    Listen(lfd, 128);                                       

    printf("Accepting client connect ...\n");
    //a
    while (1) {//如此看来多线程非常简单，只需两句话。。。
        cliaddr_len = sizeof(cliaddr);
        cfd = Accept(lfd, (struct sockaddr *)&cliaddr, &cliaddr_len);
        ts[i].cliaddr = cliaddr;
        ts[i].cfd = cfd;

        pthread_create(&tid, NULL, do_work, (void*)&ts[i]);
        pthread_detach(tid);//线程分离，子线程终止后会自动回收pcb！
        i++;
    }

    return 0;
}

