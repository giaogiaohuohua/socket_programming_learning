#include<stdio.h>
#include<unistd.h>//unlink
#include<sys/stat.h>//mkfifo
#include<fcntl.h>//open
#include<event2/event.h>

void read_cb(evutil_socket_t fd,short what,void *arg){
    char buf[1024]={0};
    int len;

    read(fd,buf,sizeof(buf));
    printf("read from write:%s\n",buf);
    sleep(1);
    return;
}

int main(void){
    int fd;
    struct event *ev=NULL; 
    
    unlink("testfifo");
    mkfifo("testfifo",0644);

    //打开fifo的读端
    fd=open("myfifo",O_RDONLY|O_NONBLOCK);
    if(-1==fd){
        exit(1);
    }

    //创建基座
    struct event_base *base=event_base_new();

    //创建事件
    ev=event_new(base,fd,EV_READ|EV_PERSIST,read_cb,NULL);

    //添加事件到基座上
    event_add(ev,NULL);

    //启动循环
    event_base_dispatch(base);

    //销毁
    event_base_free(base);
}